<?php
namespace KITT3N\Kitt3nArticle\Controller;


/***
 *
 * This file is part of the "Kitt3n | Article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * ArticleController
 */
class ArticleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * articleRepository
     * 
     * @var \KITT3N\Kitt3nArticle\Domain\Repository\ArticleRepository
     * @inject
     */
    protected $articleRepository = null;

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $articles = $this->articleRepository->findAll();
        $this->view->assign('articles', $articles);
    }

    /**
     * action show
     * 
     * @param \KITT3N\Kitt3nArticle\Domain\Model\Article $article
     * @return void
     */
    public function showAction(\KITT3N\Kitt3nArticle\Domain\Model\Article $article)
    {
        $this->view->assign('article', $article);
    }
}
