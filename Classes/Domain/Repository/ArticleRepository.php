<?php
namespace KITT3N\Kitt3nArticle\Domain\Repository;


/***
 *
 * This file is part of the "Kitt3n | Article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * The repository for Articles
 */
class ArticleRepository extends \KITT3N\Kitt3nUserfuncs\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'KITT3N\\Kitt3nArticle\\Domain\\Model\\Article';
        $sUserFuncPlugin = 'tx_kitt3narticle';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
