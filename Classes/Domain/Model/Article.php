<?php
namespace KITT3N\Kitt3nArticle\Domain\Model;


/***
 *
 * This file is part of the "Kitt3n | Article" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * Article
 */
class Article extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * input1
     * 
     * @var string
     */
    protected $input1 = '';

    /**
     * input2
     * 
     * @var string
     */
    protected $input2 = '';

    /**
     * input3
     * 
     * @var string
     */
    protected $input3 = '';

    /**
     * input4
     * 
     * @var string
     */
    protected $input4 = '';

    /**
     * input5
     * 
     * @var string
     */
    protected $input5 = '';

    /**
     * input6
     * 
     * @var string
     */
    protected $input6 = '';

    /**
     * input7
     * 
     * @var string
     */
    protected $input7 = '';

    /**
     * input8
     * 
     * @var string
     */
    protected $input8 = '';

    /**
     * input9
     * 
     * @var string
     */
    protected $input9 = '';

    /**
     * input10
     * 
     * @var string
     */
    protected $input10 = '';

    /**
     * richText1
     * 
     * @var string
     */
    protected $richText1 = '';

    /**
     * richText2
     * 
     * @var string
     */
    protected $richText2 = '';

    /**
     * richText3
     * 
     * @var string
     */
    protected $richText3 = '';

    /**
     * richText4
     * 
     * @var string
     */
    protected $richText4 = '';

    /**
     * richText5
     * 
     * @var string
     */
    protected $richText5 = '';

    /**
     * text1
     * 
     * @var string
     */
    protected $text1 = '';

    /**
     * text2
     * 
     * @var string
     */
    protected $text2 = '';

    /**
     * text3
     * 
     * @var string
     */
    protected $text3 = '';

    /**
     * text4
     * 
     * @var string
     */
    protected $text4 = '';

    /**
     * text5
     * 
     * @var string
     */
    protected $text5 = '';

    /**
     * dateTime1
     * 
     * @var \DateTime
     */
    protected $dateTime1 = null;

    /**
     * dateTime2
     * 
     * @var \DateTime
     */
    protected $dateTime2 = null;

    /**
     * dateTime3
     * 
     * @var \DateTime
     */
    protected $dateTime3 = null;

    /**
     * file1
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $file1 = null;

    /**
     * file2
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $file2 = null;

    /**
     * file3
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $file3 = null;

    /**
     * image1
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image1 = null;

    /**
     * image2
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image2 = null;

    /**
     * image3
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image3 = null;

    /**
     * slug
     * 
     * @var string
     */
    protected $slug = '';

    /**
     * Returns the input1
     * 
     * @return string $input1
     */
    public function getInput1()
    {
        return $this->input1;
    }

    /**
     * Sets the input1
     * 
     * @param string $input1
     * @return void
     */
    public function setInput1($input1)
    {
        $this->input1 = $input1;
    }

    /**
     * Returns the input2
     * 
     * @return string $input2
     */
    public function getInput2()
    {
        return $this->input2;
    }

    /**
     * Sets the input2
     * 
     * @param string $input2
     * @return void
     */
    public function setInput2($input2)
    {
        $this->input2 = $input2;
    }

    /**
     * Returns the input3
     * 
     * @return string $input3
     */
    public function getInput3()
    {
        return $this->input3;
    }

    /**
     * Sets the input3
     * 
     * @param string $input3
     * @return void
     */
    public function setInput3($input3)
    {
        $this->input3 = $input3;
    }

    /**
     * Returns the input4
     * 
     * @return string $input4
     */
    public function getInput4()
    {
        return $this->input4;
    }

    /**
     * Sets the input4
     * 
     * @param string $input4
     * @return void
     */
    public function setInput4($input4)
    {
        $this->input4 = $input4;
    }

    /**
     * Returns the input5
     * 
     * @return string $input5
     */
    public function getInput5()
    {
        return $this->input5;
    }

    /**
     * Sets the input5
     * 
     * @param string $input5
     * @return void
     */
    public function setInput5($input5)
    {
        $this->input5 = $input5;
    }

    /**
     * Returns the input6
     * 
     * @return string $input6
     */
    public function getInput6()
    {
        return $this->input6;
    }

    /**
     * Sets the input6
     * 
     * @param string $input6
     * @return void
     */
    public function setInput6($input6)
    {
        $this->input6 = $input6;
    }

    /**
     * Returns the input7
     * 
     * @return string $input7
     */
    public function getInput7()
    {
        return $this->input7;
    }

    /**
     * Sets the input7
     * 
     * @param string $input7
     * @return void
     */
    public function setInput7($input7)
    {
        $this->input7 = $input7;
    }

    /**
     * Returns the input8
     * 
     * @return string $input8
     */
    public function getInput8()
    {
        return $this->input8;
    }

    /**
     * Sets the input8
     * 
     * @param string $input8
     * @return void
     */
    public function setInput8($input8)
    {
        $this->input8 = $input8;
    }

    /**
     * Returns the input9
     * 
     * @return string $input9
     */
    public function getInput9()
    {
        return $this->input9;
    }

    /**
     * Sets the input9
     * 
     * @param string $input9
     * @return void
     */
    public function setInput9($input9)
    {
        $this->input9 = $input9;
    }

    /**
     * Returns the input10
     * 
     * @return string $input10
     */
    public function getInput10()
    {
        return $this->input10;
    }

    /**
     * Sets the input10
     * 
     * @param string $input10
     * @return void
     */
    public function setInput10($input10)
    {
        $this->input10 = $input10;
    }

    /**
     * Returns the richText1
     * 
     * @return string $richText1
     */
    public function getRichText1()
    {
        return $this->richText1;
    }

    /**
     * Sets the richText1
     * 
     * @param string $richText1
     * @return void
     */
    public function setRichText1($richText1)
    {
        $this->richText1 = $richText1;
    }

    /**
     * Returns the richText2
     * 
     * @return string $richText2
     */
    public function getRichText2()
    {
        return $this->richText2;
    }

    /**
     * Sets the richText2
     * 
     * @param string $richText2
     * @return void
     */
    public function setRichText2($richText2)
    {
        $this->richText2 = $richText2;
    }

    /**
     * Returns the richText3
     * 
     * @return string $richText3
     */
    public function getRichText3()
    {
        return $this->richText3;
    }

    /**
     * Sets the richText3
     * 
     * @param string $richText3
     * @return void
     */
    public function setRichText3($richText3)
    {
        $this->richText3 = $richText3;
    }

    /**
     * Returns the richText4
     * 
     * @return string $richText4
     */
    public function getRichText4()
    {
        return $this->richText4;
    }

    /**
     * Sets the richText4
     * 
     * @param string $richText4
     * @return void
     */
    public function setRichText4($richText4)
    {
        $this->richText4 = $richText4;
    }

    /**
     * Returns the richText5
     * 
     * @return string $richText5
     */
    public function getRichText5()
    {
        return $this->richText5;
    }

    /**
     * Sets the richText5
     * 
     * @param string $richText5
     * @return void
     */
    public function setRichText5($richText5)
    {
        $this->richText5 = $richText5;
    }

    /**
     * Returns the text1
     * 
     * @return string $text1
     */
    public function getText1()
    {
        return $this->text1;
    }

    /**
     * Sets the text1
     * 
     * @param string $text1
     * @return void
     */
    public function setText1($text1)
    {
        $this->text1 = $text1;
    }

    /**
     * Returns the text2
     * 
     * @return string $text2
     */
    public function getText2()
    {
        return $this->text2;
    }

    /**
     * Sets the text2
     * 
     * @param string $text2
     * @return void
     */
    public function setText2($text2)
    {
        $this->text2 = $text2;
    }

    /**
     * Returns the text3
     * 
     * @return string $text3
     */
    public function getText3()
    {
        return $this->text3;
    }

    /**
     * Sets the text3
     * 
     * @param string $text3
     * @return void
     */
    public function setText3($text3)
    {
        $this->text3 = $text3;
    }

    /**
     * Returns the text4
     * 
     * @return string $text4
     */
    public function getText4()
    {
        return $this->text4;
    }

    /**
     * Sets the text4
     * 
     * @param string $text4
     * @return void
     */
    public function setText4($text4)
    {
        $this->text4 = $text4;
    }

    /**
     * Returns the text5
     * 
     * @return string $text5
     */
    public function getText5()
    {
        return $this->text5;
    }

    /**
     * Sets the text5
     * 
     * @param string $text5
     * @return void
     */
    public function setText5($text5)
    {
        $this->text5 = $text5;
    }

    /**
     * Returns the dateTime1
     * 
     * @return \DateTime $dateTime1
     */
    public function getDateTime1()
    {
        return $this->dateTime1;
    }

    /**
     * Sets the dateTime1
     * 
     * @param \DateTime $dateTime1
     * @return void
     */
    public function setDateTime1(\DateTime $dateTime1)
    {
        $this->dateTime1 = $dateTime1;
    }

    /**
     * Returns the dateTime2
     * 
     * @return \DateTime $dateTime2
     */
    public function getDateTime2()
    {
        return $this->dateTime2;
    }

    /**
     * Sets the dateTime2
     * 
     * @param \DateTime $dateTime2
     * @return void
     */
    public function setDateTime2(\DateTime $dateTime2)
    {
        $this->dateTime2 = $dateTime2;
    }

    /**
     * Returns the dateTime3
     * 
     * @return \DateTime $dateTime3
     */
    public function getDateTime3()
    {
        return $this->dateTime3;
    }

    /**
     * Sets the dateTime3
     * 
     * @param \DateTime $dateTime3
     * @return void
     */
    public function setDateTime3(\DateTime $dateTime3)
    {
        $this->dateTime3 = $dateTime3;
    }

    /**
     * Returns the file1
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $file1
     */
    public function getFile1()
    {
        return $this->file1;
    }

    /**
     * Sets the file1
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file1
     * @return void
     */
    public function setFile1(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file1)
    {
        $this->file1 = $file1;
    }

    /**
     * Returns the file2
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $file2
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * Sets the file2
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file2
     * @return void
     */
    public function setFile2(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file2)
    {
        $this->file2 = $file2;
    }

    /**
     * Returns the file3
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $file3
     */
    public function getFile3()
    {
        return $this->file3;
    }

    /**
     * Sets the file3
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file3
     * @return void
     */
    public function setFile3(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file3)
    {
        $this->file3 = $file3;
    }

    /**
     * Returns the image1
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image1
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * Sets the image1
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image1
     * @return void
     */
    public function setImage1(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image1)
    {
        $this->image1 = $image1;
    }

    /**
     * Returns the image2
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image2
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Sets the image2
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image2
     * @return void
     */
    public function setImage2(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image2)
    {
        $this->image2 = $image2;
    }

    /**
     * Returns the image3
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image3
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * Sets the image3
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image3
     * @return void
     */
    public function setImage3(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image3)
    {
        $this->image3 = $image3;
    }

    /**
     * Returns the slug
     * 
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Sets the slug
     * 
     * @param string $slug
     * @return void
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }
}
