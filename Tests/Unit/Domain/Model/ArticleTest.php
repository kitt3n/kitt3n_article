<?php
namespace KITT3N\Kitt3nArticle\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 * @author ZWEI14 GmbH <hallo@ZWEI14.de>
 */
class ArticleTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nArticle\Domain\Model\Article
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nArticle\Domain\Model\Article();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getInput1ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput1()
        );
    }

    /**
     * @test
     */
    public function setInput1ForStringSetsInput1()
    {
        $this->subject->setInput1('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput2()
        );
    }

    /**
     * @test
     */
    public function setInput2ForStringSetsInput2()
    {
        $this->subject->setInput2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput3()
        );
    }

    /**
     * @test
     */
    public function setInput3ForStringSetsInput3()
    {
        $this->subject->setInput3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput4ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput4()
        );
    }

    /**
     * @test
     */
    public function setInput4ForStringSetsInput4()
    {
        $this->subject->setInput4('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput5ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput5()
        );
    }

    /**
     * @test
     */
    public function setInput5ForStringSetsInput5()
    {
        $this->subject->setInput5('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input5',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput6ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput6()
        );
    }

    /**
     * @test
     */
    public function setInput6ForStringSetsInput6()
    {
        $this->subject->setInput6('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input6',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput7ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput7()
        );
    }

    /**
     * @test
     */
    public function setInput7ForStringSetsInput7()
    {
        $this->subject->setInput7('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input7',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput8ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput8()
        );
    }

    /**
     * @test
     */
    public function setInput8ForStringSetsInput8()
    {
        $this->subject->setInput8('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input8',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput9ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput9()
        );
    }

    /**
     * @test
     */
    public function setInput9ForStringSetsInput9()
    {
        $this->subject->setInput9('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input9',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInput10ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInput10()
        );
    }

    /**
     * @test
     */
    public function setInput10ForStringSetsInput10()
    {
        $this->subject->setInput10('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'input10',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichText1ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichText1()
        );
    }

    /**
     * @test
     */
    public function setRichText1ForStringSetsRichText1()
    {
        $this->subject->setRichText1('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richText1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichText2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichText2()
        );
    }

    /**
     * @test
     */
    public function setRichText2ForStringSetsRichText2()
    {
        $this->subject->setRichText2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richText2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichText3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichText3()
        );
    }

    /**
     * @test
     */
    public function setRichText3ForStringSetsRichText3()
    {
        $this->subject->setRichText3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richText3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichText4ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichText4()
        );
    }

    /**
     * @test
     */
    public function setRichText4ForStringSetsRichText4()
    {
        $this->subject->setRichText4('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richText4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichText5ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichText5()
        );
    }

    /**
     * @test
     */
    public function setRichText5ForStringSetsRichText5()
    {
        $this->subject->setRichText5('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richText5',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText1ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText1()
        );
    }

    /**
     * @test
     */
    public function setText1ForStringSetsText1()
    {
        $this->subject->setText1('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText2ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText2()
        );
    }

    /**
     * @test
     */
    public function setText2ForStringSetsText2()
    {
        $this->subject->setText2('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText3ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText3()
        );
    }

    /**
     * @test
     */
    public function setText3ForStringSetsText3()
    {
        $this->subject->setText3('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText4ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText4()
        );
    }

    /**
     * @test
     */
    public function setText4ForStringSetsText4()
    {
        $this->subject->setText4('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text4',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getText5ReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText5()
        );
    }

    /**
     * @test
     */
    public function setText5ForStringSetsText5()
    {
        $this->subject->setText5('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text5',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime1ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime1()
        );
    }

    /**
     * @test
     */
    public function setDateTime1ForDateTimeSetsDateTime1()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime1($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime2ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime2()
        );
    }

    /**
     * @test
     */
    public function setDateTime2ForDateTimeSetsDateTime2()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime2($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateTime3ReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDateTime3()
        );
    }

    /**
     * @test
     */
    public function setDateTime3ForDateTimeSetsDateTime3()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateTime3($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'dateTime3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFile1ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getFile1()
        );
    }

    /**
     * @test
     */
    public function setFile1ForFileReferenceSetsFile1()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setFile1($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'file1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFile2ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getFile2()
        );
    }

    /**
     * @test
     */
    public function setFile2ForFileReferenceSetsFile2()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setFile2($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'file2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFile3ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getFile3()
        );
    }

    /**
     * @test
     */
    public function setFile3ForFileReferenceSetsFile3()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setFile3($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'file3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImage1ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage1()
        );
    }

    /**
     * @test
     */
    public function setImage1ForFileReferenceSetsImage1()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage1($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image1',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImage2ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage2()
        );
    }

    /**
     * @test
     */
    public function setImage2ForFileReferenceSetsImage2()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage2($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image2',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImage3ReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage3()
        );
    }

    /**
     * @test
     */
    public function setImage3ForFileReferenceSetsImage3()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage3($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image3',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSlugReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSlug()
        );
    }

    /**
     * @test
     */
    public function setSlugForStringSetsSlug()
    {
        $this->subject->setSlug('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'slug',
            $this->subject
        );
    }
}
