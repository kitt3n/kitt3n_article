<?php
namespace KITT3N\Kitt3nArticle\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 * @author ZWEI14 GmbH <hallo@ZWEI14.de>
 */
class ArticleControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nArticle\Controller\ArticleController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\KITT3N\Kitt3nArticle\Controller\ArticleController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllArticlesFromRepositoryAndAssignsThemToView()
    {

        $allArticles = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $articleRepository = $this->getMockBuilder(\KITT3N\Kitt3nArticle\Domain\Repository\ArticleRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $articleRepository->expects(self::once())->method('findAll')->will(self::returnValue($allArticles));
        $this->inject($this->subject, 'articleRepository', $articleRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('articles', $allArticles);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenArticleToView()
    {
        $article = new \KITT3N\Kitt3nArticle\Domain\Model\Article();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('article', $article);

        $this->subject->showAction($article);
    }
}
