<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nArticle',
            'Kitt3narticlelistandshow',
            [
                'Article' => 'list, show'
            ],
            // non-cacheable actions
            [
                'Article' => 'list'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    kitt3narticlelistandshow {
                        iconIdentifier = kitt3n_article-plugin-kitt3narticlelistandshow
                        title = LLL:EXT:kitt3n_article/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_article_kitt3narticlelistandshow.name
                        description = LLL:EXT:kitt3n_article/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_article_kitt3narticlelistandshow.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3narticle_kitt3narticlelistandshow
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kitt3n_article-plugin-kitt3narticlelistandshow',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_article/Resources/Public/Icons/user_plugin_kitt3narticlelistandshow.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder