#
# Table structure for table 'tx_kitt3narticle_domain_model_article'
#
CREATE TABLE tx_kitt3narticle_domain_model_article (

	input1 varchar(255) DEFAULT '' NOT NULL,
	input2 varchar(255) DEFAULT '' NOT NULL,
	input3 varchar(255) DEFAULT '' NOT NULL,
	input4 varchar(255) DEFAULT '' NOT NULL,
	input5 varchar(255) DEFAULT '' NOT NULL,
	input6 varchar(255) DEFAULT '' NOT NULL,
	input7 varchar(255) DEFAULT '' NOT NULL,
	input8 varchar(255) DEFAULT '' NOT NULL,
	input9 varchar(255) DEFAULT '' NOT NULL,
	input10 varchar(255) DEFAULT '' NOT NULL,
	rich_text1 text,
	rich_text2 text,
	rich_text3 text,
	rich_text4 text,
	rich_text5 text,
	text1 text,
	text2 text,
	text3 text,
	text4 text,
	text5 text,
	date_time1 int(11) DEFAULT '0' NOT NULL,
	date_time2 int(11) DEFAULT '0' NOT NULL,
	date_time3 int(11) DEFAULT '0' NOT NULL,
	file1 int(11) unsigned NOT NULL default '0',
	file2 int(11) unsigned NOT NULL default '0',
	file3 int(11) unsigned NOT NULL default '0',
	image1 int(11) unsigned NOT NULL default '0',
	image2 int(11) unsigned NOT NULL default '0',
	image3 int(11) unsigned NOT NULL default '0',
	slug text,

);

#
# Table structure for table 'tx_kitt3narticle_domain_model_article'
#
CREATE TABLE tx_kitt3narticle_domain_model_article (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder