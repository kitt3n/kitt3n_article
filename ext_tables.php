<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nArticle',
            'Kitt3narticlelistandshow',
            'KITT3N | Article | List/Show'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_article', 'Configuration/TypoScript', 'KITT3N | Article');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3narticle_domain_model_article', 'EXT:kitt3n_article/Resources/Private/Language/locallang_csh_tx_kitt3narticle_domain_model_article.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3narticle_domain_model_article');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder