﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
KITT3N | Article
=============================================================

.. only:: html

	:Classification:
		kitt3n_article

	:Version:
		|release|

	:Language:
		en

	:Description:

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2019

	:Author:
		Oliver Merz, Georg Kathan, Dominik Hilser, ZWEI14 GmbH

	:Email:
		o.merz@kitt3n.de, g.kathan@kitt3n.de, d.hilser@kitt3n.de, hallo@ZWEI14.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
