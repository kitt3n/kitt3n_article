<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
   'kitt3n_article',
   'tx_kitt3narticle_domain_model_article'
);
